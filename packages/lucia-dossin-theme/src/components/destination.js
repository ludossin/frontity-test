import React from "react"
import { connect, Head } from "frontity"
import Image from "@frontity/components/image";

const Destination = ({ state, libraries }) => {

	const data = state.source.get(state.router.link)
	const post = state.source[data.type][data.id]
	const Html2React = libraries.html2react.Component
	const featured = state.source.attachment[post.featured_media]

	return (
		<div>
		<Head>
		  <title>{post.title.rendered}</title>
		</Head>
		<h2>{post.title.rendered}</h2>
		<Html2React html={post.content.rendered} />
		{ featured 
	      ? <Image alt={featured.title.rendered} src={featured.source_url} />
	      : null
	     }
		</div>
	)

}

export default connect(Destination)