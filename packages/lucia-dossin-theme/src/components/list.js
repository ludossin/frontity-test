import React from "react"
import { connect, styled } from "frontity"
import Link from "@frontity/components/link"
import Switch from "@frontity/components/switch"

const List = ({ state, libraries }) => {
	const data = state.source.get(state.router.link)
	const Html2React = libraries.html2react.Component

	return (

		<Items isDestinationsArchive={data.isDestinationsArchive}>
		{data.items.map((item) => {
			const post = state.source[item.type][item.id]
			return (
				<Switch key={post.id}>
					<Destination key={item.id} when={data.isDestinationsArchive}>
						<Link link={post.link}>
						{post.title.rendered}
						</Link>
						<br/>
						<Html2React html={post.content.rendered} />
					</Destination>
					<div key={item.id}>
						<Link link={post.link}>
						{post.title.rendered}
						</Link>
						<br/>
						<Html2React html={post.content.rendered} />
					</div>
				</Switch>
			)
		})}
		</Items>

	)
}

export default connect(List)

const Items = styled.div`
	& > a {
		display: block;
		margin: 6px 0;
		font-size: 1.2em;
		color: steelblue;
		text-decoration: none;
	}
`

const Destination = styled.div`
	& > a {
		display: block;
		margin: 6px 0;
		font-size: 1.2em;
		color: maroon;
		text-decoration: none;
	}
`