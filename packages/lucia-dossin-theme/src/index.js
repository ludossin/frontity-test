import Root from "./components"
import link from "@frontity/html2react/processors/link"
import menuHandler from "./components/handlers/menu-handler";

const luciaDossin = {
  name: "lucia-dossin-theme",
  roots: {
    theme: Root,
  },
  state: {
    theme: {
      isUrlVisible: true,
      menu: [],
      menuUrl: "menu-1",
    },
  },
  actions: {
    theme: {
      toggleUrl: ({ state }) => {
        state.theme.isUrlVisible = !state.theme.isUrlVisible
      },
      beforeSSR: async ({ state, actions }) => {
        await actions.source.fetch(`/menu/${state.theme.menuUrl}/`);
      },
    },
  },
  libraries: {
    html2react: {
      processors: [link]
    },
    source: {
      handlers: [menuHandler],
    },
  }
}

export default luciaDossin
