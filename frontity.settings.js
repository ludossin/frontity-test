const settings = {
  "name": "lucia-dossin-frontity",
  "state": {
    "frontity": {
      "url": "http://wpdefault.local",
      "title": "Test Frontity Blog",
      "description": "WordPress installation for Frontity development"
    }
  },
  "packages": [
    {
      "name": "lucia-dossin-theme",
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "url": "http://wpdefault.local",
          "postTypes": [
          {
            type: "destinations", // type slug
            endpoint: "destinations", // REST API endpoint
            archive: "/destinations" // link where this custom posts are listed
          }
          ],
          "taxonomies": [
          {
            taxonomy: "regions", // taxonomy slug
            endpoint: "region", // REST API endpoint
            postTypeEndpoint: "destinations", // endpoint from which posts from this taxonomy are fetched
          }
        ]
        }
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react"
  ]
};

export default settings;
